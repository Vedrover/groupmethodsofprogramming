﻿using System;
using System.Windows;
using System.Windows.Input;

namespace МИСиУСК
{
    /// <summary>
    /// Логика взаимодействия для TestMIS.xaml
    /// </summary>
    public partial class Test : Window
    {
        private int I = 0;
        private int max;

        private int[] answers;
        private string[] questions;

        public int Max
        {
            get { return max; }
            set { max = value; }
        }
        public int[] Answers
        {
            get { return answers; }
            set { answers = value; }
        }
        public string[] Questions
        {
            get { return questions; }
            set { questions = value; }
        }
        public Test()
        {
            InitializeComponent();
            Forward.IsEnabled = false;
        }
        private void Forward1()
        {
            if (I != max)
            {
                I++;
                Count.Text = (I + 1).ToString() + "/" + (max + 1).ToString();
                Question.Text = questions[I];
                if (answers[I] == 1)
                {
                    Plus.IsEnabled = false;
                    Minus.IsEnabled = true;
                }
                else if (answers[I] == 0)
                {
                    Plus.IsEnabled = true;
                    Minus.IsEnabled = false;
                }
                else
                {
                    Plus.IsEnabled = true;
                    Minus.IsEnabled = true;
                    Forward.IsEnabled = false;
                }
                
            }
            else
            {
                //#region проверка на непройденные вопросы
                //for (int i = 0; i < answers.Length; i++)
                //{
                //    if (answers[i] == -1)
                //    {
                //        I = i;
                //        Count.Text = (I + 1).ToString() + "/" + (max + 1).ToString();
                //        Question.Text = questions[I];
                //        Plus.IsEnabled = true;
                //        Minus.IsEnabled = true;
                //        break;
                //    }
                //}
                //#endregion

                //if (I == max && (Plus.IsEnabled == false || Minus.IsEnabled == false))
                //{
                MessageBoxResult result = MessageBox.Show("Перейти к результатам?", "Завершение",
                MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    // Closes the parent form.
                    Data DataPerson = new Data();
                    bool[] answ = new bool[max + 1];
                    for (int i = 0; i < max + 1; i++)
                    {
                        if (answers[i] == 1)
                        {
                            answ[i] = true;
                        }
                        else { answ[i] = false; }

                    }
                    DataPerson.Answers = answ;
                    Close();
                    DataPerson.ShowDialog();
                }
                
               // }
            }
        }
        private void Back1()
        {
            if (I != 0)
            {
                I--;
                Count.Text = (I+1).ToString() + "/" + (max + 1).ToString();
                Question.Text = questions[I];
                if (answers[I] == 1)
                {
                    Plus.IsEnabled = false;
                    Minus.IsEnabled = true;
                    Forward.IsEnabled = true;
                }
                else if (answers[I] == 0)
                {
                    Plus.IsEnabled = true;
                    Minus.IsEnabled = false;
                    Forward.IsEnabled = true;
                }
                else
                {
                    Plus.IsEnabled = true;
                    Minus.IsEnabled = true;
                }
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Закрыть тест?", "Завершение",
                MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    MainWindow M = new MainWindow();
                    M.Show();
                    Close();
                }
            }
        }
        private void Forward_Click(object sender, RoutedEventArgs e)
        {
            Forward1();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Back1();
        }

        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            if (Plus.IsEnabled)
            {
                Plus.IsEnabled = false;
                answers[I] = 1;
                Minus.IsEnabled = true;
                Forward.IsEnabled = true;
            }
        }

        private void Minus_Click(object sender, RoutedEventArgs e)
        {
            if (Minus.IsEnabled)
            {
                Minus.IsEnabled = false;
                answers[I] = 0;
                Plus.IsEnabled = true;
                Forward.IsEnabled = true;
            }
        }
        private void KeyDownSmell(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if (Forward.IsEnabled)
                    Forward1();
            }
            if (e.Key == Key.Left)
            {
                if (Back.IsEnabled)
                    Back1();
            }

            if (e.Key == Key.Add|| e.Key == Key.OemPlus)
            {
                if (Plus.IsEnabled)
                {
                    Plus.IsEnabled = false;
                    answers[I] = 1;
                    Minus.IsEnabled = true;
                    Forward.IsEnabled = true;
                }
            }
            if (e.Key == Key.Subtract || e.Key == Key.OemMinus)
            {
                if (Minus.IsEnabled)
                {
                    Minus.IsEnabled = false;
                    answers[I] = 0;
                    Plus.IsEnabled = true;
                    Forward.IsEnabled = true;
                }
            }
        }

        private void Forward_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Right)
            {
                Forward1();
            }
            else if (e.Key == System.Windows.Input.Key.Left)
            {
                Back1();
            }
        }

        private void Grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Right)
            {
                Forward1();
            }
            else if (e.Key == System.Windows.Input.Key.Left)
            {
                Back1();
            }
        }

        private void Chit_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random();
            for (int i = 0; i < answers.Length - 1; i++)
            {
                answers[i]= r.Next(0, 2);
            }
            I = max-1;
            Forward1();
        }
    }
}
