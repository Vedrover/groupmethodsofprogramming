﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace МИСиУСК
{
    public class Person 
    {
        private string name;
        private string surname;
        private int quantityquestions;
        private DateTime birthday;
        private bool[] ans;
        private int[] mis=new int[7];
        private int[] usk = new int[9];

        private int MISclosenessStens;
        private int MISselfConfidenceStens;
        private int MISselfGuidanceStens;
        private int MISselfAttitudeStens;
        private int MISselfValueStens;
        private int MISselfAcceptanceStens;
        private int MISselfAttachmentStens;
        private int MISinternalConflictStens;
        private int MISselfBlameStens;

        private int _MIScloseness;
        private int _MISselfConfidence;
        private int _MISselfGuidance;
        private int _MISselfAttitude;
        private int _MISselfValue;
        private int _MISselfAcceptance;
        private int _MISselfAttachment;
        private int _MISinternalConflict;
        private int _MISselfBlame;

        private int USKgeneralInternality;
        private int USKinternalityOfAchievements;
        private int USKunluckyInternality;
        private int USKfamilyInternality;
        private int USKproductionInternality;
        private int USKinterpersonalInternality;
        private int USKhealthyInternality;

        private int _USKgeneralInternalityStens;
        private int _USKinternalityOfAchievementsStens;
        private int _USKunluckyInternalityStens;
        private int _USKfamilyInternalityStens;
        private int _USKproductionInternalityStens;
        private int _USKinterpersonalInternalityStens;
        private int _USKhealthyInternalityStens;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != String.Empty)
                {
                    name = value;
                }
                else
                {
                    throw new ArgumentException("Попытка установления некорректного" +
                        $"имени {value} \n");
                }
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                if (value != String.Empty)
                {
                    surname = value;
                }
                else
                {
                    throw new ArgumentException("Попытка установления некорректной" +
                        $"фамилии{value} \n");
                }
            }
        }

        public int QuantityOfQuestions 
        {
            get
            {
                return quantityquestions;
            }
            set
            {
                if (value > 0)
                {
                    quantityquestions = value;
                }
                else
                {
                    throw new ArgumentException($"Попытка установления некорректного количества вопросов {value} для теста.\n");
                }
            }
        }
        public DateTime BirthDate 
        { 
            get { return birthday; }
            set
            {
                if (value > DateTime.Now || (DateTime.Now.Year - value.Year) > 125)
                {
                    throw new ArgumentException($"Попытка установления некорректного возраста");
                }
                birthday = value;
            }
        }
        public int Age
        {
            get
            {
                if ((birthday.Month <= DateTime.Now.Month)||(birthday.Month==DateTime.Now.Month && birthday.Day>=DateTime.Now.Day))
                {
                    return DateTime.Now.Year - birthday.Year;
                }
                else
                {
                    return DateTime.Now.Year - birthday.Year - 1;
                }
            }
        }

        public bool[] Ans 
        {
            get { return ans; }
            set { ans = value; }
        }

        public int[] MIS
        {
            get { return mis; }
            set { mis = value; }
        }

        public int[] USK
        {
            get { return usk; }
            set { usk = value; }
        }
       
        public int MISClosenessStens
        {
            get { return MISclosenessStens; }
            set { MISclosenessStens = value; }
        }

        public int MISSelfConfidenceStens
        {
            get { return MISselfConfidenceStens; }
            set { MISselfConfidenceStens = value; }
        }

        public int MISSelfGuidanceStens
        {
            get { return MISselfGuidanceStens; }
            set { MISselfGuidanceStens = value; }
        }
        public int MISSelfAttitudeStens
        {
            get { return MISselfAttitudeStens; }
            set { MISselfAttitudeStens = value; }
        }
        public int MISSelfValueStens
        {
            get { return MISselfValueStens; }
            set { MISselfValueStens = value; }
        }

        public int MISSelfAcceptanceStens
        {
            get { return MISselfAcceptanceStens; }
            set { MISselfAcceptanceStens = value; }
        }
        public int MISSelfAttachmentStens
        {
            get { return MISselfAttachmentStens; }
            set { MISselfAttachmentStens = value; }
        }
        public int MISInternalConflictStens
        {
            get { return MISinternalConflictStens; }
            set { MISinternalConflictStens = value; }
        }

        public int MISSelfBlameStens
        {
            get { return MISselfBlameStens; }
            set { MISselfBlameStens = value; }
        }

        public int MIScloseness { get => _MIScloseness; set => _MIScloseness = value; }
        public int MISselfConfidence { get => _MISselfConfidence; set => _MISselfConfidence = value; }
        public int MISselfGuidance { get => _MISselfGuidance; set => _MISselfGuidance = value; }
        public int MISselfAttitude { get => _MISselfAttitude; set => _MISselfAttitude = value; }
        public int MISselfValue { get => _MISselfValue; set => _MISselfValue = value; }
        public int MISselfAcceptance { get => _MISselfAcceptance; set => _MISselfAcceptance = value; }
        public int MISselfAttachment { get => _MISselfAttachment; set => _MISselfAttachment = value; }
        public int MISinternalConflict { get => _MISinternalConflict; set => _MISinternalConflict = value; }
        public int MISselfBlame { get => _MISselfBlame; set => _MISselfBlame = value; }
        public int USKGeneralInternality
        {
            get { return USKgeneralInternality; }
            set { USKgeneralInternality = value; }
        }
        public int USKInternalityOfAchievements
        {
            get { return USKinternalityOfAchievements; }
            set { USKinternalityOfAchievements = value; }
        }
        public int USKUnluckyInternality
        {
            get { return USKunluckyInternality; }
            set { USKunluckyInternality = value; }
        }
        public int USKFamilyInternality
        {
            get { return USKfamilyInternality; }
            set { USKfamilyInternality = value; }
        }
        public int USKProductionInternality
        {
            get { return USKproductionInternality; }
            set { USKproductionInternality = value; }
        }
        public int USKInterpersonalInternality
        {
            get { return USKinterpersonalInternality; }
            set { USKinterpersonalInternality = value; }
        }
        public int USKHealthyInternality
        {
            get { return USKhealthyInternality; }
            set { USKhealthyInternality = value; }
        }

       
        public int USKgeneralInternalityStens { get => _USKgeneralInternalityStens; set => _USKgeneralInternalityStens = value; }
        public int USKinternalityOfAchievementsStens { get => _USKinternalityOfAchievementsStens; set => _USKinternalityOfAchievementsStens = value; }
        public int USKunluckyInternalityStens { get => _USKunluckyInternalityStens; set => _USKunluckyInternalityStens = value; }
        public int USKfamilyInternalityStens { get => _USKfamilyInternalityStens; set => _USKfamilyInternalityStens = value; }
        public int USKproductionInternalityStens { get => _USKproductionInternalityStens; set => _USKproductionInternalityStens = value; }
        public int USKinterpersonalInternalityStens { get => _USKinterpersonalInternalityStens; set => _USKinterpersonalInternalityStens = value; }
        public int USKhealthyInternalityStens { get => _USKhealthyInternalityStens; set => _USKhealthyInternalityStens = value; }

        public Person(string _name, string _surname, int _quantityofquestions, DateTime _birtdate)
        {
            Name = _name;
            Surname = _surname;
            QuantityOfQuestions = _quantityofquestions;
            BirthDate = new DateTime(_birtdate.Ticks);
            Ans = new bool[QuantityOfQuestions];
        }
    }
}
