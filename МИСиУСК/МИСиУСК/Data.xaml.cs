﻿using System;
using System.Windows;
namespace МИСиУСК
{
    /// <summary>
    /// Логика взаимодействия для Data.xaml
    /// </summary>
    public partial class Data : Window
    {
        private Person person;
        private bool[] answers;

        public bool[] Answers
        {
            get { return answers; }
            set { answers = value; }
        }
        public Data()
        {
            InitializeComponent();
        }

        private void Result_Click(object sender, RoutedEventArgs e)
        {
            if (FamilyBox.Text != "" && NameBox.Text!="" && DateBox.Text != "" && FamilyBox.Text[0] != ' ' && NameBox.Text[0] != ' ' && DateBox.DisplayDate <= DateTime.Now) 
            {
                try 
                {
                    person = new Person(NameBox.Text, FamilyBox.Text, answers.Length, DateBox.DisplayDate);
                    if (answers.Length == 44)
                    {
                        
                        string res = USKTestsAnalysis.USKresult(answers, person);

                        //USKTable.TableUSK(person);
                        USKTable.ExcelTableUSK(person);
                        Result R = new Result();
                        R.P1.Value = person.USKgeneralInternalityStens;
                        R.nNP1.Text = person.USKgeneralInternalityStens.ToString() + "/10";
                        R.NP1.Text = "Интеральность общая";
                        R.P2.Value = person.USKinternalityOfAchievementsStens;
                        R.nNP2.Text = person.USKinternalityOfAchievementsStens + "/10";
                        R.NP2.Text = "Интеральность достижений";
                        R.P3.Value = person.USKunluckyInternalityStens;
                        R.nNP3.Text = person.USKunluckyInternalityStens.ToString() + "/10";
                        R.NP3.Text = "Интеральность неудач";
                        R.P4.Value = person.USKfamilyInternalityStens;
                        R.nNP4.Text = person.USKfamilyInternalityStens.ToString() + "/10";
                        R.NP4.Text = "Интеральность семьи";
                        R.P5.Value = person.USKproductionInternalityStens;
                        R.nNP5.Text = person.USKproductionInternalityStens.ToString() + "/10";
                        R.NP5.Text = "Производственная интеральность";
                        R.P6.Value = person.USKinterpersonalInternalityStens;
                        R.nNP6.Text = person.USKinterpersonalInternalityStens.ToString() + "/10";
                        R.NP6.Text = "Межличностная интеральность";
                        R.P7.Value = person.USKhealthyInternalityStens;
                        R.nNP7.Text = person.USKhealthyInternalityStens.ToString() + "/10";
                        R.NP7.Text = "интеральность здоровья";

                        R.P8.Visibility = Visibility.Hidden;
                        R.P9.Visibility = Visibility.Hidden;


                        R.ResultBlock.Text = res;
                        Close();
                        R.ShowDialog();
                       
                    }
                    else
                    {
                        string res = MISTestsAnalysis.MISresult(answers, person);
                        MISTable.FillTable(person);
                        Result R = new Result();
                        //R.P1.Value = person.MISClosenessStens;
                        //R.NP1.Text = "з\nа\nк\nр\nы\nт\nо\nс\nт\nь";
                        //R.P2.Value = person.MISSelfConfidenceStens;
                        //R.NP2.Text = "с\nа\nм\nо\nу\nв\nе\nр\nе\nн\nн\nо\nс\nт\nь";
                        //R.P3.Value = person.MISSelfGuidanceStens;
                        //R.NP3.Text = "с\nа\nм\nо\nр\nу\nк\nо\nв\nо\nд\nс\nт\nв\nо";
                        //R.P4.Value = person.MISSelfAttitudeStens;
                        //R.NP4.Text = "о\nт\nр\nа\nж\nе\nн\nн\nо\nе\n \nC.\nO.";
                        //R.P5.Value = person.MISSelfValueStens;
                        //R.NP5.Text = "с\nа\nм\nо\nц\nе\nн\nн\nо\nс\nт\nь";
                        //R.P6.Value = person.MISSelfAcceptanceStens;
                        //R.NP6.Text = "с\nа\nм\nо\nп\nр\nи\nн\nя\nт\nи\nе";
                        //R.P7.Value = person.MISSelfAttachmentStens;
                        //R.NP7.Text = "с\nа\nм\nо\nп\nр\nи\nв\nя\nз\nа\nн\nн\nо\nс\nть";
                        //R.P8.Value = person.MISInternalConflictStens;
                        //R.NP8.Text = "в\nн\nу\nт\nр.\n \nк\nо\nн\nф\nл.";
                        //R.P9.Value = person.MISSelfBlameStens;
                        //R.NP9.Text = "с\nа\nм\nо\nо\nб\nв\nи\nн\nе\nн\nи\nе";
                        R.P1.Value = person.MISClosenessStens;
                        R.nNP1.Text= person.MISClosenessStens.ToString()+"/10";
                        R.NP1.Text = "Закрытость";
                        R.P2.Value = person.MISSelfConfidenceStens;
                        R.nNP2.Text = person.MISSelfConfidenceStens+"/10";
                        R.NP2.Text = "Самоуверенность";
                        R.P3.Value = person.MISSelfGuidanceStens;
                        R.nNP3.Text = person.MISSelfGuidanceStens.ToString() + "/10";
                        R.NP3.Text = "Саморуководство";
                        R.P4.Value = person.MISSelfAttitudeStens;
                        R.nNP4.Text = person.MISSelfAttitudeStens.ToString() + "/10";
                        R.NP4.Text = "Отраженное самоотношение";
                        R.P5.Value = person.MISSelfValueStens;
                        R.nNP5.Text = person.MISSelfValueStens.ToString() + "/10";
                        R.NP5.Text = "Самоценность";
                        R.P6.Value = person.MISSelfAcceptanceStens;
                        R.nNP6.Text = person.MISSelfAcceptanceStens.ToString() + "/10";
                        R.NP6.Text = "Самопринятие";
                        R.P7.Value = person.MISSelfAttachmentStens;
                        R.nNP7.Text = person.MISSelfAttachmentStens.ToString() + "/10";
                        R.NP7.Text = "Самопривязанность";
                        R.P8.Value = person.MISInternalConflictStens;
                        R.nNP8.Text = person.MISInternalConflictStens.ToString() + "/10";
                        R.NP8.Text = "Внутренняя конфликтность";
                        R.P9.Value = person.MISSelfBlameStens;
                        R.nNP9.Text = person.MISSelfBlameStens.ToString() + "/10";
                        R.NP9.Text = "Самообвинение";

                        R.ResultBlock.Text = res;
                        Close();
                        R.ShowDialog();
                    }
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Некорректная дата рождения!", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                }
                
            }else if(FamilyBox.Text == "")
            {
                //MessageBox.Show("Введите фамилию!");
                MessageBox.Show("Введите фамилию!", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (NameBox.Text == "" )
            {
                MessageBox.Show("Введите Имя!", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }else if(DateBox.Text == "")
            {
                MessageBox.Show("Введите дату рождения!", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }else if(FamilyBox.Text[0] == ' ')
            {
                MessageBox.Show("Фамилия не может начинаться на 'пробел'!", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if( NameBox.Text[0] == ' ')
            {
                MessageBox.Show("Имя не может начинаться на 'пробел'!", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);

            }
            else if( DateBox.DisplayDate > DateTime.Now)
            {
                MessageBox.Show("Дата рождения не может быть в будущем!", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }

        }
    }
}
