﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace МИСиУСК
{
    class USKTable
    {
//        static string[] tableTop = { "Фамилия", "Имя", "Возраст", "Общая Интернальность(ИО)", "Интернальность Достижений(ИД)", "Интернальность Неудач(ИН)", "Семейная Интернальность(ИС)", "Производственная интернальность(ИП)", "Межличностная Интернальность(ИМ)", "Интернальность здоровья(ИЗ)" };
        
        public static void ExcelTableUSK(Person p)
        {
            if (!File.Exists("USKResult.xlsx"))
            {
                var package = new ExcelPackage(new System.IO.FileInfo("USKResult.xlsx"));
                var sheetFresh = package.Workbook.Worksheets.Add("Fresh");
                sheetFresh.Cells[1, 1, 1, 10].LoadFromArrays(new object[][] { new[] { "Фамилия", "Имя", "Возраст", "Общая Интернальность", "Интернальность Достижений", "Интернальность Неудач", "Семейная Интернальность", "Производственная интернальность", "Межличностная Интернальность", "Интернальность здоровья" } } );
                int row = sheetFresh.Dimension.End.Row + 1;
                sheetFresh.Cells[row, 1].Value = "Фамилия";
                sheetFresh.Cells[row, 2].Value = "Имя";
                sheetFresh.Cells[row, 3].Value = "Возраст";
                sheetFresh.Cells[row, 4].Value = "ИО";
                sheetFresh.Cells[row, 5].Value = "ИД";
                sheetFresh.Cells[row, 6].Value = "ИН";
                sheetFresh.Cells[row, 7].Value = "ИС";
                sheetFresh.Cells[row, 8].Value = "ИП";
                sheetFresh.Cells[row, 9].Value = "ИМ";
                sheetFresh.Cells[row, 10].Value = "ИЗ";
                row = sheetFresh.Dimension.End.Row + 1;
                sheetFresh.Cells[row, 1].Value = p.Surname;
                sheetFresh.Cells[row, 2].Value = p.Name;
                sheetFresh.Cells[row, 3].Value = p.Age;
                sheetFresh.Cells[row, 4].Value = p.USKGeneralInternality;
                sheetFresh.Cells[row, 5].Value = p.USKInternalityOfAchievements;
                sheetFresh.Cells[row, 6].Value = p.USKUnluckyInternality;
                sheetFresh.Cells[row, 7].Value = p.USKFamilyInternality;
                sheetFresh.Cells[row, 8].Value = p.USKProductionInternality;
                sheetFresh.Cells[row, 9].Value = p.USKInterpersonalInternality;
                sheetFresh.Cells[row, 10].Value = p.USKHealthyInternality;

                for (int i = 1; i < 11; i++)
                {
                    sheetFresh.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheetFresh.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheetFresh.Column(i).AutoFit();
                }

                var sheetStens = package.Workbook.Worksheets.Add("Stens");
                sheetStens.Cells[1, 1, 1, 10].LoadFromArrays(new object[][] { new[] { "Фамилия", "Имя", "Возраст", "Общая Интернальность", "Интернальность Достижений", "Интернальность Неудач", "Семейная Интернальность", "Производственная интернальность", "Межличностная Интернальность", "Интернальность здоровья" } });
                row = sheetStens.Dimension.End.Row + 1;
                sheetStens.Cells[row, 1].Value = "Фамилия";
                sheetStens.Cells[row, 2].Value = "Имя";
                sheetStens.Cells[row, 3].Value = "Возраст";
                sheetStens.Cells[row, 4].Value = "ИО";
                sheetStens.Cells[row, 5].Value = "ИД";
                sheetStens.Cells[row, 6].Value = "ИН";
                sheetStens.Cells[row, 7].Value = "ИС";
                sheetStens.Cells[row, 8].Value = "ИП";
                sheetStens.Cells[row, 9].Value = "ИМ";
                sheetStens.Cells[row, 10].Value = "ИЗ";
 
                row = sheetStens.Dimension.End.Row + 1;
                sheetStens.Cells[row, 1].Value = p.Surname;
                sheetStens.Cells[row, 2].Value = p.Name;
                sheetStens.Cells[row, 3].Value = p.Age;
                sheetStens.Cells[row, 4].Value = p.USKgeneralInternalityStens;
                sheetStens.Cells[row, 5].Value = p.USKinternalityOfAchievementsStens;
                sheetStens.Cells[row, 6].Value = p.USKunluckyInternalityStens;
                sheetStens.Cells[row, 7].Value = p.USKfamilyInternalityStens;
                sheetStens.Cells[row, 8].Value = p.USKproductionInternalityStens;
                sheetStens.Cells[row, 9].Value = p.USKinterpersonalInternalityStens;
                sheetStens.Cells[row, 10].Value = p.USKhealthyInternalityStens;

                for (int i = 1; i < 11; i++)
                {
                    sheetStens.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheetStens.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheetStens.Column(i).AutoFit();
                }
                package.SaveAs(new System.IO.FileInfo("USKResult.xlsx"));
            }
            else
            {
                var package = new ExcelPackage(new System.IO.FileInfo("USKResult.xlsx"));
                var sheetFresh = package.Workbook.Worksheets[1];
                int row = sheetFresh.Dimension.End.Row + 1;

                sheetFresh.Cells[row, 1].Value = p.Surname;
                sheetFresh.Cells[row, 2].Value = p.Name;
                sheetFresh.Cells[row, 3].Value = p.Age;
                sheetFresh.Cells[row, 4].Value = p.USKGeneralInternality;
                sheetFresh.Cells[row, 5].Value = p.USKInternalityOfAchievements;
                sheetFresh.Cells[row, 6].Value = p.USKUnluckyInternality;
                sheetFresh.Cells[row, 7].Value = p.USKFamilyInternality;
                sheetFresh.Cells[row, 8].Value = p.USKProductionInternality;
                sheetFresh.Cells[row, 9].Value = p.USKInterpersonalInternality;
                sheetFresh.Cells[row, 10].Value = p.USKHealthyInternality;

                var sheetStens = package.Workbook.Worksheets[2];
                row = sheetStens.Dimension.End.Row + 1;
                sheetStens.Cells[row, 1].Value = p.Surname;
                sheetStens.Cells[row, 2].Value = p.Name;
                sheetStens.Cells[row, 3].Value = p.Age;
                sheetStens.Cells[row, 4].Value = p.USKgeneralInternalityStens;
                sheetStens.Cells[row, 5].Value = p.USKinternalityOfAchievementsStens;
                sheetStens.Cells[row, 6].Value = p.USKunluckyInternalityStens;
                sheetStens.Cells[row, 7].Value = p.USKfamilyInternalityStens;
                sheetStens.Cells[row, 8].Value = p.USKproductionInternalityStens;
                sheetStens.Cells[row, 9].Value = p.USKinterpersonalInternalityStens;
                sheetStens.Cells[row, 10].Value = p.USKhealthyInternalityStens;
                package.SaveAs(new System.IO.FileInfo("USKResult.xlsx"));

            }
        }
    }
}
