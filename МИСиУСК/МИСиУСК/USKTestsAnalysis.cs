﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace МИСиУСК
{
    static class USKTestsAnalysis
    {   
        //
        static private int[] GeneralInternalityPlusKey = { 2, 4, 11, 12, 13, 15, 16, 17, 19, 20, 22, 25, 27, 29, 31, 32, 34, 36, 37, 39, 42, 44 };
        static private int[] GeneralInternalityMinusKey = { 1, 3, 5, 6, 7, 8, 9, 10, 14, 18, 21, 23, 24, 26, 28, 33, 38, 40, 41, 43, 30, 35 };

        static private int[] InternalityOfAchievementsPlusKey = { 12, 15, 27, 32, 36, 37 };
        static private int[] InternalityOfAchievementsMinusKey = { 1, 5, 6, 14, 26, 43 };

        static private int[] UnluckyInternalityPlusKey = { 2, 4, 20, 31, 42, 44 };
        static private int[] UnluckyInternalityMinusKey = { 7, 24, 33, 38, 40, 41 };

        static private int[] FamilyInternalityPlusKey = { 2, 16, 20, 32, 37 };
        static private int[] FamilyInternalityMinusKey = { 7, 14, 26, 28, 41 };

        static private int[] ProductionInternalityPlusKey = { 19, 22, 25, 42 };
        static private int[] ProductionInternalityMinusKey = { 1, 9, 10, 30 };

        static private int[] InterpersonalInternalityPlusKey = { 4, 27 };
        static private int[] InterpersonalInternalityMinusKey = { 6, 38 };

        static private int[] HealthyInternalityPlusKey = { 13, 34 };
        static private int[] HealthyInternalityMinusKey = { 3, 23 };

        static public string USKresult(bool[] answers, Person p)
        {
            string res = "";
            int GeneralInternality = 0;
            int InternalityOfAchievements = 0;
            int UnluckyInternality = 0;
            int FamilyInternality = 0;
            int ProductionInternality = 0;
            int InterpersonalInternality = 0;
            int HealthyInternality = 0;

            foreach (int i in GeneralInternalityPlusKey)
            {
                if (answers[i - 1])
                {
                    GeneralInternality++;
                }
            }
            foreach (int i in GeneralInternalityMinusKey)
            {
                if (!answers[i - 1])
                {
                    GeneralInternality++;
                }
            }

            foreach (int i in InternalityOfAchievementsPlusKey)
            {
                if (answers[i - 1])
                {
                    InternalityOfAchievements++;
                }
            }
            foreach (int i in InternalityOfAchievementsMinusKey)
            {
                if (!answers[i - 1])
                {
                    InternalityOfAchievements++;
                }
            }

            foreach (int i in UnluckyInternalityPlusKey)
            {
                if (answers[i - 1])
                {
                    UnluckyInternality++;
                }
            }
            foreach (int i in UnluckyInternalityMinusKey)
            {
                if (!answers[i - 1])
                {
                    UnluckyInternality++;
                }
            }

            foreach (int i in FamilyInternalityPlusKey)
            {
                if (answers[i - 1])
                {
                    FamilyInternality++;
                }
            }
            foreach (int i in FamilyInternalityMinusKey)
            {
                if (!answers[i - 1])
                {
                    FamilyInternality++;
                }
            }

            foreach (int i in ProductionInternalityPlusKey)
            {
                if (answers[i - 1])
                {
                    ProductionInternality++;
                }
            }
            foreach (int i in ProductionInternalityMinusKey)
            {
                if (!answers[i - 1])
                {
                    ProductionInternality++;
                }
            }

            foreach (int i in InterpersonalInternalityPlusKey)
            {
                if (answers[i - 1])
                {
                    InterpersonalInternality++;
                }
            }
            foreach (int i in InterpersonalInternalityMinusKey)
            {
                if (!answers[i - 1])
                {
                    InterpersonalInternality++;
                }
            }

            foreach (int i in HealthyInternalityPlusKey)
            {
                if (answers[i - 1])
                {
                    HealthyInternality++;
                }
            }
            foreach (int i in HealthyInternalityMinusKey)
            {
                if (!answers[i - 1])
                {
                    HealthyInternality++;
                }
            }
            p.USKGeneralInternality = GeneralInternality;
            p.USKInternalityOfAchievements = InternalityOfAchievements;
            p.USKFamilyInternality = FamilyInternality;
            p.USKHealthyInternality = HealthyInternality;
            p.USKInterpersonalInternality = InterpersonalInternality;
            p.USKUnluckyInternality = UnluckyInternality;
            p.USKProductionInternality = ProductionInternality;
            #region GenIntSten
            if (GeneralInternality >= 0 && GeneralInternality <= 4)
            {
                p.USKgeneralInternalityStens = 1;
            }
            else if (GeneralInternality >= 5 && GeneralInternality <= 8)
            {
                p.USKgeneralInternalityStens = 2;
            }
            else if (GeneralInternality >= 9 && GeneralInternality <= 11)
            {
                p.USKgeneralInternalityStens = 3;
            }
            else if (GeneralInternality >= 12 && GeneralInternality <= 16)
            {
                p.USKgeneralInternalityStens = 4;
            }
            else if (GeneralInternality >= 17 && GeneralInternality <= 21)
            {
                p.USKgeneralInternalityStens = 5;
            }
            else if (GeneralInternality >= 22 && GeneralInternality <= 26)
            {
                p.USKgeneralInternalityStens = 6;
            }
            else if (GeneralInternality >= 28 && GeneralInternality <= 32)
            {
                p.USKgeneralInternalityStens = 7;
            }
            else if (GeneralInternality >= 33 && GeneralInternality <= 36)
            {
                p.USKgeneralInternalityStens = 8;
            }
            else if (GeneralInternality >= 37 && GeneralInternality <= 40)
            {
                p.USKgeneralInternalityStens = 9;
            }
            else if (GeneralInternality >= 41 && GeneralInternality <= 44)
            {
                p.USKgeneralInternalityStens = 10;
            }

            if (p.USKgeneralInternalityStens >= 0 && p.USKgeneralInternalityStens <= 3)
            {
                res += $"Интернальность общая (ИО) соответствуют высокому уровню субъективного контроля над любыми значимыми ситуациями. Такие люди считают, что большинство важных событий в их жизни было результатом их собственных действий, что они могут ими управлять и, следовательно, берут на себя ответственность за свою жизнь в целом\n";
            }
            else if (p.USKgeneralInternalityStens >= 4 && p.USKgeneralInternalityStens <= 7)
            {
                res += $"Интернальность общая (ИО) характеризуют ситуативное проявление интернального и экстернального контроля\n";
            }
            else if (p.USKgeneralInternalityStens >= 8 && p.USKgeneralInternalityStens <= 10)
            {
                res += $"Интернальность общая (ИО) соответствует низкому уровню субъективного контроля. Такие люди не видят связи между своими действиями и значимыми событиями, которые они рассматривают как результат случая или действия других людей.\n";
            }
            res += "Ваше значение: " + p.USKgeneralInternalityStens + "\n";
            res += "------------------ \n";
            #endregion
            #region IntOfAchSten
            if (InternalityOfAchievements >= 0 && InternalityOfAchievements <= 1)
            {
                p.USKinternalityOfAchievementsStens = 1;
            }
            else if (InternalityOfAchievements == 2)
            {
                p.USKinternalityOfAchievementsStens = 2;
            }
            else if (InternalityOfAchievements == 3)
            {
                p.USKinternalityOfAchievementsStens = 3;
            }
            else if (InternalityOfAchievements == 4)
            {
                p.USKinternalityOfAchievementsStens = 4;
            }
            else if (InternalityOfAchievements >= 5 && InternalityOfAchievements <= 6)
            {
                p.USKinternalityOfAchievementsStens = 5;
            }
            else if (InternalityOfAchievements == 7)
            {
                p.USKinternalityOfAchievementsStens = 6;
            }
            else if (InternalityOfAchievements == 8)
            {
                p.USKinternalityOfAchievementsStens = 7;
            }
            else if (InternalityOfAchievements >= 9 && InternalityOfAchievements <= 10)
            {
                p.USKinternalityOfAchievementsStens = 8;
            }
            else if (InternalityOfAchievements == 11)
            {
                p.USKinternalityOfAchievementsStens = 9;
            }
            else if (InternalityOfAchievements == 12)
            {
                p.USKinternalityOfAchievementsStens = 10;
            }

            if (p.USKinternalityOfAchievementsStens >= 0 && p.USKinternalityOfAchievementsStens <= 3)
            {
                res += $"Интернальность достижений (ИД)  соответствуют высокому уровню субъективного контроля над эмоционально положительными событиями. Такие люди считают, что всего самого хорошего в своей жизни они добились сами и что они способны с успехом идти к намеченой цели в будущем.\n";
            }
            else if (p.USKinternalityOfAchievementsStens >= 4 && p.USKinternalityOfAchievementsStens <= 7)
            {
                res += $"Интернальность достижений (ИД) характеризуют ситуативное проявление интернального и экстернального контроля\n";
            }
            else if (p.USKinternalityOfAchievementsStens >= 8 && p.USKinternalityOfAchievementsStens <= 10)
            {
                res += $"Интернальность достижений (ИД) свидетельствуют о том, что человек связывает свои успехи, достижения и радости с внешними обстоятельствами - везением, счастливой судьбой или помощью других людей\n";
            }

            res += "Ваше значение: " + p.USKinternalityOfAchievementsStens + "\n";

            res +=  "------------------ \n";
            #endregion
            #region UnlIntSten
            if (UnluckyInternality >= 0 && UnluckyInternality <= 1)
            {
                p.USKunluckyInternalityStens = 1;
            }
            else if (UnluckyInternality == 2)
            {
                p.USKunluckyInternalityStens = 2;
            }
            else if (UnluckyInternality == 3)
            {
                p.USKunluckyInternalityStens = 3;
            }
            else if (UnluckyInternality == 4)
            {
                p.USKunluckyInternalityStens = 4;
            }
            else if (UnluckyInternality >= 5 && UnluckyInternality <= 6)
            {
                p.USKunluckyInternalityStens = 5;
            }
            else if (UnluckyInternality == 7)
            {
                p.USKunluckyInternalityStens = 6;
            }
            else if (UnluckyInternality == 8)
            {
                p.USKunluckyInternalityStens = 7;
            }
            else if (UnluckyInternality >= 9 && UnluckyInternality <= 10)
            {
                p.USKunluckyInternalityStens = 8;
            }
            else if (UnluckyInternality == 11)
            {
                p.USKunluckyInternalityStens = 9;
            }
            else if (UnluckyInternality == 12)
            {
                p.USKunluckyInternalityStens = 10;
            }

            if (p.USKunluckyInternalityStens >= 0 && p.USKunluckyInternalityStens <= 3)
            {
                res += $"Интернальность неудач (ИН) говорят о развитом чувстве субъективного контроля по отношению к отрицательным событиям и ситуациям, что проявляется в склонности обвинять самого себя в разнообразных неприятностях и неудачах.\n";
            }
            else if (p.USKunluckyInternalityStens >= 4 && p.USKunluckyInternalityStens <= 7)
            {
                res += $"Интернальность неудач (ИН) характеризуют ситуативное проявление интернального и экстернального контроля\n";
            }
            else if (p.USKunluckyInternalityStens >= 8 && p.USKunluckyInternalityStens <= 10)
            {
                res += $"Интернальность неудач (ИН) соответствует низкому уровню субъективного контроля. Такие люди не видят связи между своими действиями и значимыми событиями, которые они рассматривают как результат случая или действия других людей.\n";
            }
            res += "Ваше значение: " + p.USKunluckyInternalityStens + "\n";
            res += "------------------ \n";
            #endregion
            #region FamIntSten
            if (FamilyInternality >= 0 && FamilyInternality <= 1)
            {
                p.USKfamilyInternalityStens = 1;
            }
            else if (FamilyInternality == 2)
            {
                p.USKfamilyInternalityStens = 2;
            }
            else if (FamilyInternality == 3)
            {
                p.USKfamilyInternalityStens = 3;
            }
            else if (FamilyInternality == 4)
            {
                p.USKfamilyInternalityStens = 4;
            }
            else if (FamilyInternality == 5)
            {
                p.USKfamilyInternalityStens = 5;
            }
            else if (FamilyInternality == 6)
            {
                p.USKfamilyInternalityStens = 6;
            }
            else if (FamilyInternality == 7)
            {
                p.USKfamilyInternalityStens = 7;
            }
            else if (FamilyInternality == 8)
            {
                p.USKfamilyInternalityStens = 8;
            }
            else if (FamilyInternality == 9)
            {
                p.USKfamilyInternalityStens = 9;
            }
            else if (FamilyInternality == 10)
            {
                p.USKfamilyInternalityStens = 10;
            }

            if (p.USKfamilyInternalityStens >= 0 && p.USKfamilyInternalityStens <= 3)
            {
                res += $"Интернальность семьи (ИС) означают, что человек считает себя ответственным за события, происходящие в его семейной жизни\n";
            }
            else if (p.USKfamilyInternalityStens >= 4 && p.USKfamilyInternalityStens <= 7)
            {
                res += $"Интернальность семьи (ИС) характеризуют ситуативное проявление интернального и экстернального контроля\n";
            }
            else if (p.USKfamilyInternalityStens >= 8 && p.USKfamilyInternalityStens <= 10)
            {
                res += $"Интернальность семьи (ИС) указывают на то, что субъект считает своих партнеров причиной значимых ситуаций, возникающих в его семье.\n";
            }
            res += "Ваше значение: " + p.USKfamilyInternalityStens + "\n";
            res += "------------------ \n";
            #endregion
            #region ProdIntSten
            if (ProductionInternality >= 0 && ProductionInternality <= 1)
            {
                p.USKproductionInternalityStens = 1;
            }
            else if (ProductionInternality == 2)
            {
                p.USKproductionInternalityStens = 3;
            }
            else if (ProductionInternality == 3)
            {
                p.USKproductionInternalityStens = 4;
            }
            else if (ProductionInternality == 4)
            {
                p.USKproductionInternalityStens = 5;
            }
            else if (ProductionInternality == 5)
            {
                p.USKproductionInternalityStens = 6;
            }
            else if (ProductionInternality == 6)
            {
                p.USKproductionInternalityStens = 7;
            }
            else if (ProductionInternality == 7)
            {
                p.USKproductionInternalityStens = 8;
            }
            else if (ProductionInternality == 8)
            {
                p.USKproductionInternalityStens = 10;
            }

            if (p.USKproductionInternalityStens >= 0 && p.USKproductionInternalityStens <= 3)
            {
                res += $"Интернальность производственных отношений (ИП) свидетельствуют о том, что человек считает себя, свои действия важным фактором организации собственной производственной деятельности, в частности, в своем продвижении по службе\n";
            }
            else if (p.USKproductionInternalityStens >= 4 && p.USKproductionInternalityStens <= 7)
            {
                res += $"Интернальность производственных отношений (ИП) характеризуют ситуативное проявление интернального и экстернального контроля\n";
            }
            else if (p.USKproductionInternalityStens >= 8 && p.USKproductionInternalityStens <= 10)
            {
                res += $"Интернальность производственных отношений (ИП) указывают на склонность придавать более важное значение внешним обстоятельствам - руководству, коллегам по работе, везению - невезению\n";
            }

            res += "Ваше значение: " + p.USKproductionInternalityStens + "\n";
            res += "------------------ \n";
            #endregion
            #region IntIntSten
            if (InterpersonalInternality == 0)
            {
                p.USKinterpersonalInternalityStens = 1;
            }
            else if (InterpersonalInternality == 1)
            {
                p.USKinterpersonalInternalityStens = 3;
            }
            else if (InterpersonalInternality == 2)
            {
                p.USKinterpersonalInternalityStens = 5;
            }
            else if (InterpersonalInternality == 3)
            {
                p.USKinterpersonalInternalityStens = 8;
            }
            else if (InterpersonalInternality == 4)
            {
                p.USKinterpersonalInternalityStens = 10;
            }

            if (p.USKinterpersonalInternalityStens >= 0 && p.USKinterpersonalInternalityStens <= 3)
            {
                res += $"Интернальность межличностных отношений (ИМ) свидетельствуют о том, что человек чувствует себя способным вызывать уважение и симпатию других людей\n";
            }
            else if (p.USKinterpersonalInternalityStens >= 4 && p.USKinterpersonalInternalityStens <= 7)
            {
                res += $"Интернальность межличностных отношений (ИМ) характеризуют ситуативное проявление интернального и экстернального контроля\n";
            }
            else if (p.USKinterpersonalInternalityStens >= 8 && p.USKinterpersonalInternalityStens <= 10)
            {
                res += $"Интернальность межличностных отношений (ИМ) указывают на то, что субъект не склонен брать на себя ответственность за свои отношения с окружающими\n";
            }
            res += "Ваше значение: " + p.USKinterpersonalInternalityStens + "\n";
            res += "------------------ \n";
            #endregion
            #region IntHealSten
            if (HealthyInternality == 0)
            {
                p.USKhealthyInternalityStens = 1;
            }
            else if (HealthyInternality == 1)
            {
                p.USKhealthyInternalityStens = 3;
            }
            else if (HealthyInternality == 2)
            {
                p.USKhealthyInternalityStens = 5;
            }
            else if (HealthyInternality == 3)
            {
                p.USKhealthyInternalityStens = 8;
            }
            else if (HealthyInternality == 4)
            {
                p.USKhealthyInternalityStens = 10;
            }

            if (p.USKhealthyInternalityStens >= 0 && p.USKhealthyInternalityStens <= 3)
            {
                res += $"Интернальность здоровья (ИЗ) свидетельствуют о том, что человек считает себя во многом ответственным за свое здоровье и полагает, что выздоровление зависит преимущественно от его действий \n";
            }
            else if (p.USKhealthyInternalityStens >= 4 && p.USKhealthyInternalityStens <= 7)
            {
                res += $"Интернальность здоровья (ИЗ) характеризуют ситуативное проявление интернального и экстернального контроля\n";
            }
            else if (p.USKhealthyInternalityStens >= 8 && p.USKhealthyInternalityStens <= 10)
            {
                res += $"Интернальность здоровья (ИЗ) соответствуют представлению о том, что здоровье и болезнь являются результатом случая и надежде на то, что выздоровление придет в результате действий других людей, прежде всего врачей.\n";
            }
            res += "Ваше значение: " + p.USKhealthyInternalityStens + "\n";
            res += "------------------ \n";
            #endregion
            return res;
        }

        public static readonly string[] Questions =
            {
            "1.Продвижение по службе больше зависит от удачного стечения обстоятельств, чем от способностей и усилий человека.",
            "2.Большинство разводов происходит от того, что люди не захотели приспособиться друг к другу.",
            "3.Болезнь - дело случая: если уж суждено заболеть, то ничего не поделаешь",
            "4.Люди оказываются одинокими из-за того, что сами не проявляют интереса и дружелюбия к окружающим.",
            "5.Осуществление моих желаний часто зависит от везения.",
            "6.Бесполезно предпринимать усилия для того, чтобы завоевать симпатию других людей.",
            "7.Внешние обстоятельства - родители и благосостояние - влияют на семейное счастье не меньше, чем отношения супругов.",
            "8.Я часто чувствую, что мало влияю на то, что происходит со мной.",
            "9.Как правило, руководство оказывается более эффективным, когда полностью контролирует действия подчиненных, а не полагается на их самостоятельность.",
            "10.Мои отметки в школе зависели от случайных обстоятельств (например, настроения учителя) больше, чем от моих собственных усилий.",
            "11.Когда я строю планы, то я, в общем, верю, что смогу их осуществить.",
            "12.То, что многим людям кажется удачей или везением, на самом деле является результатом долгих целенаправленных усилий.",
            "13.Думаю, что правильный образ жизни может больше помочь здоровью, чем врачи и лекарства.",
            "14.Если люди не подходят друг другу, то как бы они ни старались наладить семейную жизнь, они все равно не смогут это сделать.",
            "15.То хорошее, что я делаю, обычно бывает по достоинству оценено другими.",
            "16.Дети вырастают такими, какими их воспитывают родители.",
            "17.Думаю, что случай или судьба не играют важной роли в моей жизни.",
            "18.Я стараюсь не планировать далеко вперед, потому что многое зависит от того, как сложатся обстоятельства",
            "19.Мои отметки в школе больше всего зависели от моих усилий и степени подготовленности.",
            "20.В семейных конфликтах я чаще чувствую вину за собой, чем за противоположной стороной.",
            "21.Жизнь большинства людей зависит от стечения обстоятельств.",
            "22.Я предпочитаю такое руководство, при котором можно самостоятельно определять, что и как делать.",
            "23.Думаю, что мой образ жизни ни в коей мере не является причиной моих болезней.",
            "24.Как правило, именно неудачное стечение обстоятельств мешает людям добиться успеха в своем деле.",
            "25.В конце концов, за плохое управление организацией ответственны сами люди, которые в ней работают.",
            "26.Я часто чувствую, что ничего не могу изменить в сложившихся отношениях в семье.",
            "27.Если я очень захочу, то смогу расположить к себе почти любого.",
            "28.На подрастающее поколение влияет так много разных обстоятельств, что усилия родителей по их воспитанию часто оказываются бесполезными.",
            "29.То, что со мной случается, это дело моих собственных рук.",
            "30.Человек, который не смог добиться успеха в своей работе, скорее всего не проявил достаточных усилий.",
            "31.Трудно бывает понять, почему руководители поступают именно так, а не иначе.",
            "32.Чаще всего я могу добиться от членов моей семьи того, чего хочу.",
            "33.В неприятностях и неудачах, которые были в моей жизни, чаще всего виноваты другие люди, чем я сам",
            "34.Ребенка всегда можно уберечь от простуды, если за ним следить и правильно его одевать",
            "35.В сложных обстоятельствах я предпочитаю подождать, пока проблемы не решатся сами собой",
            "36.Успех является результатом упорной работы и мало зависит от случая или везения.",
            "37.Я чувствую, что от меня больше, чем от кого бы то ни было, зависит счастье моей семьи.",
            "38.Мне всегда трудно понять, почему я нравлюсь одним людям и не нравлюсь другим.",
            "39.Я всегда предпочитаю принять решение и действовать самостоятельно, а не надеяться на помощь других людей или на судьбу.",
            "40.К сожалению, заслуги человека часто остаются непризнанными, несмотря на все его старания.",
            "41.В семейной жизни бывают такие случаи, когда невозможно разрешить проблемы даже при самом сильном желании",
            "42.Способные люди, не сумевшие реализовать свои возможности, должны винить в этом только самих себя.",
            "43.Многие мои успехи были возможны только благодаря помощи других людей.",
            "44.Большинство неудач в моей жизни произошло от неумения, незнания или лени и мало зависело от везения или невезения."
        };
    }
}