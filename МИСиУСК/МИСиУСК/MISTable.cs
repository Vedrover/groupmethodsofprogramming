﻿using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace МИСиУСК
{
    class MISTable
    {
        public static void FillTable(Person p)
        {
            if (!File.Exists("MISResult.xlsx"))
            {
                var package = new ExcelPackage(new System.IO.FileInfo("MISResult.xlsx"));
                var sheetFresh = package.Workbook.Worksheets.Add("Fresh");
                sheetFresh.Cells[1, 1, 1, 12].LoadFromArrays(new object[][] { new[] { "Фамилия", "Имя", "Возраст", "Замкнутость", "Самоуверенность", "Саморуководство", "Отраженное взаимоотношение", "Самоценность", "Самопринятие", "Самопривязанность", "Внутренняя конфликность", "Самообвинение" } } );
                int row = sheetFresh.Dimension.End.Row + 1;
                sheetFresh.Cells[row, 1].Value = "Фамилия";
                sheetFresh.Cells[row, 2].Value = "Имя";
                sheetFresh.Cells[row, 3].Value = "Возраст";
                sheetFresh.Cells[row, 4].Value = "З";
                sheetFresh.Cells[row, 5].Value = "Су";
                sheetFresh.Cells[row, 6].Value = "Ср";
                sheetFresh.Cells[row, 7].Value = "ОС";
                sheetFresh.Cells[row, 8].Value = "Сц";
                sheetFresh.Cells[row, 9].Value = "Спр";
                sheetFresh.Cells[row, 10].Value = "Спз";
                sheetFresh.Cells[row, 11].Value = "ВК";
                sheetFresh.Cells[row, 12].Value = "Со";
                row = sheetFresh.Dimension.End.Row + 1;
                sheetFresh.Cells[row, 1].Value = p.Surname;
                sheetFresh.Cells[row, 2].Value = p.Name;
                sheetFresh.Cells[row, 3].Value = p.Age;
                sheetFresh.Cells[row, 4].Value = p.MIScloseness;
                sheetFresh.Cells[row, 5].Value = p.MISselfConfidence;
                sheetFresh.Cells[row, 6].Value = p.MISselfGuidance;
                sheetFresh.Cells[row, 7].Value = p.MISselfAttitude;
                sheetFresh.Cells[row, 8].Value = p.MISselfValue;
                sheetFresh.Cells[row, 9].Value = p.MISselfAcceptance;
                sheetFresh.Cells[row, 10].Value = p.MISselfAttachment;
                sheetFresh.Cells[row, 11].Value = p.MISinternalConflict;
                sheetFresh.Cells[row, 12].Value = p.MISselfBlame;

                for (int i = 1; i < 13; i++)
                {
                    sheetFresh.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheetFresh.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheetFresh.Column(i).AutoFit();
                }

                var sheetStens = package.Workbook.Worksheets.Add("Stens");
                sheetStens.Cells[1, 1, 1, 12].LoadFromArrays(new object[][] { new[] { "Фамилия", "Имя", "Возраст", "Замкнутость", "Самоуверенность", "Саморуководство", "Отраженное взаимоотношение", "Самоценность", "Самопринятие", "Самопривязанность", "Внутренняя конфликность", "Самообвинение" } } );
                row = sheetStens.Dimension.End.Row + 1;
                sheetStens.Cells[row, 1].Value = "Фамилия";
                sheetStens.Cells[row, 2].Value = "Имя";
                sheetStens.Cells[row, 3].Value = "Возраст";
                sheetStens.Cells[row, 4].Value = "З";
                sheetStens.Cells[row, 5].Value = "Су";
                sheetStens.Cells[row, 6].Value = "Ср";
                sheetStens.Cells[row, 7].Value = "ОС";
                sheetStens.Cells[row, 8].Value = "Сц";
                sheetStens.Cells[row, 9].Value = "Спр";
                sheetStens.Cells[row, 10].Value = "Спз";
                sheetStens.Cells[row, 11].Value = "ВК";
                sheetStens.Cells[row, 12].Value = "Со";
                row = sheetStens.Dimension.End.Row + 1;
                sheetStens.Cells[row, 1].Value = p.Surname;
                sheetStens.Cells[row, 2].Value = p.Name;
                sheetStens.Cells[row, 3].Value = p.Age;
                sheetStens.Cells[row, 4].Value = p.MISClosenessStens;
                sheetStens.Cells[row, 5].Value = p.MISSelfConfidenceStens;
                sheetStens.Cells[row, 6].Value = p.MISSelfGuidanceStens;
                sheetStens.Cells[row, 7].Value = p.MISSelfAttitudeStens;
                sheetStens.Cells[row, 8].Value = p.MISSelfValueStens;
                sheetStens.Cells[row, 9].Value = p.MISSelfAcceptanceStens;
                sheetStens.Cells[row, 10].Value = p.MISSelfAttachmentStens;
                sheetStens.Cells[row, 11].Value = p.MISInternalConflictStens;
                sheetStens.Cells[row, 12].Value = p.MISSelfBlameStens;                 
                for (int i = 1; i < 13; i++)
                {
                    sheetStens.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheetStens.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheetStens.Column(i).AutoFit();
                }
                package.SaveAs(new System.IO.FileInfo("MISResult.xlsx"));
            }
            else
            {
                var package = new ExcelPackage(new System.IO.FileInfo("MISResult.xlsx"));
                var sheetFresh = package.Workbook.Worksheets[1];
                int row = sheetFresh.Dimension.End.Row + 1;

                sheetFresh.Cells[row, 1].Value = p.Surname;
                sheetFresh.Cells[row, 2].Value = p.Name;
                sheetFresh.Cells[row, 3].Value = p.Age;
                sheetFresh.Cells[row, 4].Value = p.MIScloseness;
                sheetFresh.Cells[row, 5].Value = p.MISselfConfidence;
                sheetFresh.Cells[row, 6].Value = p.MISselfGuidance;
                sheetFresh.Cells[row, 7].Value = p.MISselfAttitude;
                sheetFresh.Cells[row, 8].Value = p.MISselfValue;
                sheetFresh.Cells[row, 9].Value = p.MISselfAcceptance;
                sheetFresh.Cells[row, 10].Value = p.MISselfAttachment;
                sheetFresh.Cells[row, 11].Value = p.MISinternalConflict;
                sheetFresh.Cells[row, 12].Value = p.MISselfBlame;
                var sheetStens = package.Workbook.Worksheets[2];
                row = sheetStens.Dimension.End.Row + 1;
                sheetStens.Cells[row, 1].Value = p.Surname;
                sheetStens.Cells[row, 2].Value = p.Name;
                sheetStens.Cells[row, 3].Value = p.Age;
                sheetStens.Cells[row, 4].Value = p.MISClosenessStens;
                sheetStens.Cells[row, 5].Value = p.MISSelfConfidenceStens;
                sheetStens.Cells[row, 6].Value = p.MISSelfGuidanceStens;
                sheetStens.Cells[row, 7].Value = p.MISSelfAttitudeStens;
                sheetStens.Cells[row, 8].Value = p.MISSelfValueStens;
                sheetStens.Cells[row, 9].Value = p.MISSelfAcceptanceStens;
                sheetStens.Cells[row, 10].Value = p.MISSelfAttachmentStens;
                sheetStens.Cells[row, 11].Value = p.MISInternalConflictStens;
                sheetStens.Cells[row, 12].Value = p.MISSelfBlameStens;
                package.SaveAs(new System.IO.FileInfo("MISResult.xlsx"));

            }
        }
    }
}

