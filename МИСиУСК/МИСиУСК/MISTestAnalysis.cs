﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace МИСиУСК
{
    static class MISTestsAnalysis
    {

        #region Test keys
        static private int[] ClosenessPlusKey = { 1, 3, 9, 48, 53, 56, 65 };
        static private int[] ClosenessMinusKey = { 21, 62, 86, 98 };

        static private int[] SelfConfidencePlusKey = { 7, 24, 30, 35, 36, 51, 52, 58, 61, 73, 82 };
        static private int[] SelfConfidenceMinusKey = { 20, 80, 103 };

        static private int[] SelfGuidancePlusKey = { 43, 44, 45, 74, 76, 84, 90, 105, 106, 108, 110 };
        static private int[] SelfGuidanceMinusKey = { 109 };

        static private int[] SelfAttitudePlusKey = { 2, 5, 29, 41, 42, 50, 102 };
        static private int[] SelfAttitudeMinusKey = { 13, 18, 34, 85 };

        static private int[] SelfValuePlusKey = { 8, 16, 39, 54, 57, 68, 70, 75, 100 };
        static private int[] SelfValueMinusKey = { 15, 26, 31, 46, 83 };

        static private int[] SelfAcceptancePlusKey = { 10, 12, 17, 28, 40, 49, 63, 72, 77, 79, 88, 97 };
        static private int[] SelfAcceptanceMinusKey = { -1 };

        static private int[] SelfAttachmentPlusKey = { 6, 32, 33, 55, 89, 93, 95, 101, 104 };
        static private int[] SelfAttachmentMinusKey = { 96, 107 };

        static private int[] InternalConflictPlusKey = { 4, 11, 22, 23, 27, 38, 47, 59, 64, 67, 69, 81, 91, 94, 99 };
        static private int[] InternalConflictMinusKey = { -1 };

        static private int[] SelfBlamePlusKey = { 14, 19, 25, 37, 60, 66, 71, 78, 87, 92 };
        static private int[] SelfBlameMinusKey = { -1 };
        #endregion
        //This method processes answers and returns test scales, what do they mean and what is your result in each scale (as text)
        static public string MISresult(bool[] answers, Person p)
        {
            //Test scales
            int Closeness = 0;
            int SelfConfidence = 0;
            int SelfGuidance = 0;
            int SelfAttitude = 0;
            int SelfValue = 0;
            int SelfAcceptance = 0;
            int SelfAttachment = 0;
            int InternalConflict = 0;
            int SelfBlame = 0;
            // Test

            #region Scale processing
            //Closeness scale processing
            foreach (int i in ClosenessPlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) Closeness++;
                }
            }
            foreach (int i in ClosenessMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) Closeness++;
                }
            }


            //SelfConfidence scale processing
            foreach (int i in SelfConfidencePlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) SelfConfidence++;
                }
            }
            foreach (int i in SelfConfidenceMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) SelfConfidence++;
                }
            }


            //SelfGuidance scale processing
            foreach (int i in SelfGuidancePlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) SelfGuidance++;
                }
            }
            foreach (int i in SelfGuidanceMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) SelfGuidance++;
                }
            }


            //SelfAttitude scale processing
            foreach (int i in SelfAttitudePlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) SelfAttitude++;
                }
            }
            foreach (int i in SelfAttitudeMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) SelfAttitude++;
                }
            }


            //SelfValue scale processing
            foreach (int i in SelfValuePlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) SelfValue++;
                }
            }
            foreach (int i in SelfValueMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) SelfValue++;
                }
            }


            //SelfAcceptance scale processing
            foreach (int i in SelfAcceptancePlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) SelfAcceptance++;
                }
            }
            foreach (int i in SelfAcceptanceMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) SelfAcceptance++;
                }
            }


            //SelfAttachment scale processing
            foreach (int i in SelfAttachmentPlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) SelfAttachment++;
                }
            }
            foreach (int i in SelfAttachmentMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) SelfAttachment++;
                }
            }


            //InternalConflict scale processing
            foreach (int i in InternalConflictPlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) InternalConflict++;
                }
            }
            foreach (int i in InternalConflictMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) InternalConflict++;
                }
            }


            //SelfBlame scale processing
            foreach (int i in SelfBlamePlusKey)
            {
                if (i > 0)
                {
                    if (answers[i - 1]) SelfBlame++;
                }
            }
            foreach (int i in SelfBlameMinusKey)
            {
                if (i > 0)
                {
                    if (!answers[i - 1]) SelfBlame++;
                }
            }
            #endregion  

            #region Transferring raw points into standart ones

            p.MIScloseness = Closeness;
            p.MISselfConfidence = SelfConfidence;
            p.MISselfGuidance = SelfGuidance;
            p.MISselfAttitude = SelfAttitude;
            p.MISselfValue = SelfValue;
            p.MISselfAcceptance = SelfAcceptance;
            p.MISselfAttachment = SelfAttachment;
            p.MISinternalConflict = InternalConflict;
            p.MISselfBlame = SelfBlame;


            int StCloseness = 0;
            int StSelfConfidence = 0;
            int StSelfGuidance = 0;
            int StSelfAttitude = 0;
            int StSelfValue = 0;
            int StSelfAcceptance = 0;
            int StSelfAttachment = 0;
            int StInternalConflict = 0;
            int StSelfBlame = 0;

            //Closeness
            switch (Closeness)
            {
                case 0:
                    StCloseness = 1;
                    break;
                case 1:
                    StCloseness = 3;
                    break;
                case 2:
                    StCloseness = 4;
                    break;
                case 3:
                    StCloseness = 4;
                    break;
                case 4:
                    StCloseness = 5;
                    break;
                case 5:
                    StCloseness = 5;
                    break;
                case 6:
                    StCloseness = 6;
                    break;
                case 7:
                    StCloseness = 6;
                    break;
                case 8:
                    StCloseness = 7;
                    break;
                case 9:
                    StCloseness = 8;
                    break;
                case 10:
                    StCloseness = 9;
                    break;
                case 11:
                    StCloseness = 10;
                    break;
                default:
                    break;
            }


            //SelfConfidence
            switch (SelfConfidence)
            {
                case 0:
                    StSelfConfidence = 1;
                    break;
                case 1:
                    StSelfConfidence = 1;
                    break;
                case 2:
                    StSelfConfidence = 2;
                    break;
                case 3:
                    StSelfConfidence = 3;
                    break;
                case 4:
                    StSelfConfidence = 3;
                    break;
                case 5:
                    StSelfConfidence = 4;
                    break;
                case 6:
                    StSelfConfidence = 4;
                    break;
                case 7:
                    StSelfConfidence = 5;
                    break;
                case 8:
                    StSelfConfidence = 5;
                    break;
                case 9:
                    StSelfConfidence = 6;
                    break;
                case 10:
                    StSelfConfidence = 7;
                    break;
                case 11:
                    StSelfConfidence = 7;
                    break;
                case 12:
                    StSelfConfidence = 8;
                    break;
                case 13:
                    StSelfConfidence = 9;
                    break;
                case 14:
                    StSelfConfidence = 10;
                    break;
                default:
                    break;
            }


            //SelfGuidance
            switch (SelfGuidance)
            {
                case 0:
                    StSelfGuidance = 1;
                    break;
                case 1:
                    StSelfGuidance = 1;
                    break;
                case 2:
                    StSelfGuidance = 2;
                    break;
                case 3:
                    StSelfGuidance = 3;
                    break;
                case 4:
                    StSelfGuidance = 4;
                    break;
                case 5:
                    StSelfGuidance = 4;
                    break;
                case 6:
                    StSelfGuidance = 5;
                    break;
                case 7:
                    StSelfGuidance = 6;
                    break;
                case 8:
                    StSelfGuidance = 7;
                    break;
                case 9:
                    StSelfGuidance = 8;
                    break;
                case 10:
                    StSelfGuidance = 8;
                    break;
                case 11:
                    StSelfGuidance = 9;
                    break;
                case 12:
                    StSelfGuidance = 10;
                    break;
                default:
                    break;
            }


            //SelfAttitude
            switch (SelfAttitude)
            {
                case 0:
                    StSelfAttitude = 1;
                    break;
                case 1:
                    StSelfAttitude = 2;
                    break;
                case 2:
                    StSelfAttitude = 3;
                    break;
                case 3:
                    StSelfAttitude = 4;
                    break;
                case 4:
                    StSelfAttitude = 4;
                    break;
                case 5:
                    StSelfAttitude = 5;
                    break;
                case 6:
                    StSelfAttitude = 6;
                    break;
                case 7:
                    StSelfAttitude = 6;
                    break;
                case 8:
                    StSelfAttitude = 7;
                    break;
                case 9:
                    StSelfAttitude = 8;
                    break;
                case 10:
                    StSelfAttitude = 9;
                    break;
                case 11:
                    StSelfAttitude = 10;
                    break;
                default:
                    break;
            }


            //SelfValue
            switch (SelfValue)
            {
                case 0:
                    StSelfValue = 1;
                    break;
                case 1:
                    StSelfValue = 1;
                    break;
                case 2:
                    StSelfValue = 2;
                    break;
                case 3:
                    StSelfValue = 3;
                    break;
                case 4:
                    StSelfValue = 4;
                    break;
                case 5:
                    StSelfValue = 4;
                    break;
                case 6:
                    StSelfValue = 5;
                    break;
                case 7:
                    StSelfValue = 5;
                    break;
                case 8:
                    StSelfValue = 6;
                    break;
                case 9:
                    StSelfValue = 7;
                    break;
                case 10:
                    StSelfValue = 7;
                    break;
                case 11:
                    StSelfValue = 8;
                    break;
                case 12:
                    StSelfValue = 9;
                    break;
                case 13:
                    StSelfValue = 10;
                    break;
                case 14:
                    StSelfValue = 10;
                    break;
                default:
                    break;
            }


            //SelfAcceptance
            switch (SelfAcceptance)
            {
                case 0:
                    StSelfAcceptance = 1;
                    break;
                case 1:
                    StSelfAcceptance = 1;
                    break;
                case 2:
                    StSelfAcceptance = 2;
                    break;
                case 3:
                    StSelfAcceptance = 3;
                    break;
                case 4:
                    StSelfAcceptance = 3;
                    break;
                case 5:
                    StSelfAcceptance = 4;
                    break;
                case 6:
                    StSelfAcceptance = 5;
                    break;
                case 7:
                    StSelfAcceptance = 5;
                    break;
                case 8:
                    StSelfAcceptance = 6;
                    break;
                case 9:
                    StSelfAcceptance = 7;
                    break;
                case 10:
                    StSelfAcceptance = 8;
                    break;
                case 11:
                    StSelfAcceptance = 9;
                    break;
                case 12:
                    StSelfAcceptance = 10;
                    break;
                default:
                    break;
            }


            //SelfAttachment
            switch (SelfAttachment)
            {
                case 0:
                    StSelfAttachment = 1;
                    break;
                case 1:
                    StSelfAttachment = 2;
                    break;
                case 2:
                    StSelfAttachment = 3;
                    break;
                case 3:
                    StSelfAttachment = 4;
                    break;
                case 4:
                    StSelfAttachment = 5;
                    break;
                case 5:
                    StSelfAttachment = 5;
                    break;
                case 6:
                    StSelfAttachment = 6;
                    break;
                case 7:
                    StSelfAttachment = 7;
                    break;
                case 8:
                    StSelfAttachment = 7;
                    break;
                case 9:
                    StSelfAttachment = 8;
                    break;
                case 10:
                    StSelfAttachment = 9;
                    break;
                case 11:
                    StSelfAttachment = 10;
                    break;
                default:
                    break;
            }


            //InternalConflict
            switch (InternalConflict)
            {
                case 0:
                    StInternalConflict = 1;
                    break;
                case 1:
                    StInternalConflict = 2;
                    break;
                case 2:
                    StInternalConflict = 3;
                    break;
                case 3:
                    StInternalConflict = 4;
                    break;
                case 4:
                    StInternalConflict = 4;
                    break;
                case 5:
                    StInternalConflict = 5;
                    break;
                case 6:
                    StInternalConflict = 5;
                    break;
                case 7:
                    StInternalConflict = 5;
                    break;
                case 8:
                    StInternalConflict = 6;
                    break;
                case 9:
                    StInternalConflict = 6;
                    break;
                case 10:
                    StInternalConflict = 6;
                    break;
                case 11:
                    StInternalConflict = 7;
                    break;
                case 12:
                    StInternalConflict = 7;
                    break;
                case 13:
                    StInternalConflict = 8;
                    break;
                case 14:
                    StInternalConflict = 9;
                    break;
                case 15:
                    StInternalConflict = 10;
                    break;
                default:
                    break;
            }


            //SelfBlame
            switch (SelfBlame)
            {
                case 0:
                    StSelfBlame = 1;
                    break;
                case 1:
                    StSelfBlame = 2;
                    break;
                case 2:
                    StSelfBlame = 3;
                    break;
                case 3:
                    StSelfBlame = 4;
                    break;
                case 4:
                    StSelfBlame = 4;
                    break;
                case 5:
                    StSelfBlame = 5;
                    break;
                case 6:
                    StSelfBlame = 6;
                    break;
                case 7:
                    StSelfBlame = 6;
                    break;
                case 8:
                    StSelfBlame = 7;
                    break;
                case 9:
                    StSelfBlame = 8;
                    break;
                case 10:
                    StSelfBlame = 10;
                    break;
                default:
                    break;
            }
            #endregion


            p.MISClosenessStens = StCloseness;
            p.MISSelfConfidenceStens = StSelfConfidence;
            p.MISSelfGuidanceStens = StSelfGuidance;
            p.MISSelfAttitudeStens = StSelfAttitude;
            p.MISSelfValueStens = StSelfValue;
            p.MISSelfAcceptanceStens = StSelfAcceptance;
            p.MISSelfAttachmentStens = StSelfAttachment;
            p.MISInternalConflictStens = StInternalConflict;
            p.MISSelfBlameStens = StSelfBlame;
            //Scales stringBuilders for output
//            StringBuilder ClosenessSB = new StringBuilder($"Шкала \"Закрытость\" определяет преобладание одной из двух тенденций: либо конформности, выраженной мотивации социального одобрения, либо критичности, глубоко осознания себя, внутренней честности и открытости. \nВаше значение = {StCloseness}");
//           StringBuilder SelfConfidenceSB = new StringBuilder($"Шкала \"Самоуверенность\" выявляет самоуважение, отношение к себе как к уверенному, самостоятельному, волевому и надежному человеку, который знает, что ему есть за что себя уважать.\nВаше значение = {StSelfConfidence}");
 //           StringBuilder SelfGuidanceSB = new StringBuilder($"Шкала \"Саморуководство\" отражает представление личности об основном источнике собственной активности, результатов и достижений, об источнике развития собственной личности, подчеркивает доминирование либо собственного \"Я\", либо внешних обстоятельств.\nВаше значение = {StSelfGuidance}");
//           StringBuilder SelfAttitudeSB = new StringBuilder($"Шкала \"Отраженное самоотношение\" характеризует представление субъекта о способности вызвать у других людей уважение, симпатию. При интерпретации необходимо учитывать, что шкала не отражает истинного содержания взаимодействия между людьми, это лишь субъективное восприятие сложившихся отношений.\nВаше значение = {StSelfAttitude}");
 //           StringBuilder SelfValueSB = new StringBuilder($"Шкала \"Самоценность\" передает ощущение ценности собственной личности и предполагаемую ценность собственного \"Я\" для других.\nВаше значение = {StSelfValue}");
 //          StringBuilder SelfAcceptanceSB = new StringBuilder($"Шкала \"Самопринятие\" позволяет судить о выраженности чувства симпатии к себе, согласия со своими внутренними побуждениями, принятия себя таким, какой есть, несмотря на недостатки и слабости.\nВаше значение = {StSelfAcceptance}");
 //           StringBuilder SelfAttachmentSB = new StringBuilder($"Шкала \"Самопривязанность\" выявляет степень желания изменяться по отношению к наличному состоянию.\nВаше значение = {StSelfAttachment}");
 //           StringBuilder InternalConflictSB = new StringBuilder($"Шкала \"Внутренняя конфликтность\" определяет наличие внутренних конфликтов, сомнений, несогласия с собой, выраженность тенденций к самокопанию и рефлексии.\nВаше значение = {StInternalConflict}");
 //          StringBuilder SelfBlameSB = new StringBuilder($"Шкала \"Самообвинение\" характеризует выраженность отрицательных эмоций в адрес своего \"Я\".\nВаше значение = {StSelfBlame}");

            StringBuilder ClosenessSB = new StringBuilder($"");
            StringBuilder SelfConfidenceSB = new StringBuilder($"");
            StringBuilder SelfGuidanceSB = new StringBuilder($"");
            StringBuilder SelfAttitudeSB = new StringBuilder($"");
            StringBuilder SelfValueSB = new StringBuilder($"");
            StringBuilder SelfAcceptanceSB = new StringBuilder($"");
            StringBuilder SelfAttachmentSB = new StringBuilder($"");
            StringBuilder InternalConflictSB = new StringBuilder($"");
            StringBuilder SelfBlameSB = new StringBuilder($"");

            ClosenessSB.Append("Закрытость:");
            switch (StCloseness)
            {
                case int _ when StCloseness <= 3:
                    {
                        ClosenessSB.Append("\n Низкие значения (1-3 стена) указывают на внутреннюю честность, на открытость отношений человека с самим собой, на достаточно развитую рефлексию и глубокое понимание себя. Человек критичен по отношению к себе. Во взаимоотношениях с людьми доминирует ориентация на собственное видение ситуации, происходящего.");
                        break;
                    }
                case int _ when StCloseness <= 7:
                    {
                        ClosenessSB.Append("\n Средние значения (4-7 стенов) означают избирательное отношение человека к себе; преодоление некоторых психологических защит при актуализации других, особенно в критических ситуациях.");
                        break;
                    }
                case int _ when StCloseness <= 10:
                    {
                        ClosenessSB.Append("\n Высокие значения (8-10 стенов) отражают выраженное защитное поведение личности, желание соответствовать общепринятым нормам поведение и взаимоотношений с окружающими людьми. Человек склонен избегать открытых отношений с самим собой; причиной может быть или недостаточность навыков рефлексии, поверхностное видение себя, или осознанное нежелание раскрывать себя, признавать существование личных проблем. ");
                        break;
                    }
            }
            ClosenessSB.Append("\n");
            ClosenessSB.Append("--------------------\n");

            ClosenessSB.Append("Самоуверенность:");
            switch (StSelfConfidence)
            {
                case int _ when StSelfConfidence <= 3:
                    {
                        SelfConfidenceSB.Append("\n Низкие значения (1-3 стена) отражают неуважение к себе, связанное с неуверенностью в своих возможностях, с сомнением в своих способностях. Человек не доверяет своим решениям, часто сомневается в способности преодолевать трудности и препятствия, достигать намеченные цели. Возможны избегание контактов с людьми, глубокое погружение в собственные проблемы, внутренняя напряженность.");
                        break;
                    }
                case int _ when StSelfConfidence <= 7:
                    {
                        SelfConfidenceSB.Append("\n Средние значения (4-7 стенов) свойственны тем, кто в привычных для себя ситуациях сохраняет работоспособность, уверенность в себе, ориентацию на успех начинаний. При неожиданном появлении трудностей уверенность в себе снижается, нарастают тревога, беспокойство.");
                        break;
                    }
                case int _ when StSelfConfidence <= 10:
                    {
                        SelfConfidenceSB.Append("\n Высокие значения (8-10 стенов) характеризуют выраженную самоуверенность, ощущение силы собственного \"Я\", высокую смелость в общении. Доминирует мотив успеха. Человек уважает себя, доволен собой, своими начинаниями и достижениями, ощущает свою компетентность и способность решать многие жизненные вопросы. Препятствия на пути к достижению цели воспринимаются как преодолимые. Проблемы затрагивают неглубоко, переживаются недолго. ");
                        break;
                    }
            }
            SelfConfidenceSB.Append("\n");
            SelfConfidenceSB.Append("--------------------\n");

            SelfGuidanceSB.Append("Саморуководство:");
            switch (StSelfGuidance)
            {
                case int _ when StSelfGuidance <= 3:
                    {
                        SelfGuidanceSB.Append("\n Низкие значения (1-3 стена) описывают веру субъекта в подвластность своего \"Я\" внешним обстоятельствам и событиям. Механизмы саморегуляции ослаблены. Волевой контроль недостаточен для преодоления внешних и внутренних препятствий на пути к достижению цели. Основным источником происходящего с человеком признаются внешние обстоятельства. Причины, заключающиеся в себе, или отрицаются, или, что встречается довольно часто, вытесняются в подсознание. Переживания относительно собственного \"Я\" сопровождаются внутренним напряжением.");
                        break;
                    }
                case int _ when StSelfGuidance <= 7:
                    {
                        SelfGuidanceSB.Append("\n Средние значения (4-7 стенов) раскрывают особенности отношения к своему \"Я\" в зависимости от степени адаптированности в ситуации. В привычных для себя условиях существования, в которых все возможные изменения знакомы и хорошо прогнозируемы, человек может проявлять выраженную способность к личному контролю. В новых для себя ситуациях регуляционные возможности \"Я\" ослабевают, усиливается склонность к подчинению средовым воздействиям. ");
                        break;
                    }
                case int _ when StSelfGuidance <= 10:
                    {
                        SelfGuidanceSB.Append("\n Высокие значения (8-10 стенов) характерны для тех, кто основным источником развития своей личности, регулятором достижений и успехов считает себя. Человек переживает собственное \"Я\" как внутренний стержень, который координирует и направляет всю активность, организует поведение и отношения с людьми, что делает его способным прогнозировать свои действия и последствия возникающих контактов с окружающими. Он ощущает себя способным оказывать сопротивление внешним влияниям, противиться судьбе и стихии событий. Человеку свойствен контроль над эмоциональными реакциями и переживаниями по поводу себя. ");
                        break;
                    }
            }
            SelfGuidanceSB.Append("\n");
            SelfGuidanceSB.Append("--------------------\n");

            SelfAttitudeSB.Append("Отраженное самоотношение:");
            switch (StSelfAttitude)
            {
                case int _ when StSelfAttitude <= 3:
                    {
                        SelfAttitudeSB.Append("\n Низкие значения (1-3 стена) указывают на то, что человек относится к себе как к неспособному вызвать уважение у окружающих, как к вызывающему у других людей осуждение и порицание. Одобрение, поддержка от других не ожидаются.");
                        break;
                    }
                case int _ when StSelfAttitude <= 7:
                    {
                        SelfAttitudeSB.Append("\n Средние значения (4-7 стенов) означают избирательное восприятие человеком отношения окружающих к себе. С его точки зрения, положительное отношение окружающих распространяется лишь на определенные качества, на определенные поступки; другие личностные проявления способны вызывать у них раздражение и непринятие.");
                        break;
                    }
                case int _ when StSelfAttitude <= 10:
                    {
                        SelfAttitudeSB.Append("\n Высокие значения (8-10 стенов) соответствуют человеку, который воспринимает себя принятым окружающими людьми. Он чувствует, что его любят другие, ценят за личностные и духовные качества, за совершаемые поступки и действия, за приверженность групповым нормам и правилам. Он ощущает в себе общительность, эмоциональную открытость для взаимодействия с окружающими, легкость установления деловых и личных контактов. ");
                        break;
                    }
            }
            SelfAttitudeSB.Append("\n");
            SelfAttitudeSB.Append("--------------------\n");

            SelfValueSB.Append("Самоценность:");
            switch (StSelfValue)
            {
                case int _ when StSelfValue <= 3:
                    {
                        SelfValueSB.Append("\n Низкие значения (1-3 стена) говорят о глубоких сомнениях человека в уникальности своей личности, недооценке своего духовного \"Я\". Неуверенность в себе ослабляет сопротивление средовым влияниям. Повышенная чувствительность к замечаниям и критике окружающих в свой адрес делает человека обидчивым и ранимым, склонным не доверять своей индивидуальности.");
                        break;
                    }
                case int _ when StSelfValue <= 7:
                    {
                        SelfValueSB.Append("\n Средние значения (4-7 стенов) отражают избирательное отношение к себе. Человек склонен высоко оценивать ряд своих качеств, признавать их уникальность. Другие же качества явно недооцениваются, поэтому замечания окружающих могут вызвать ощущение малоценности, личной несостоятельности.");
                        break;
                    }
                case int _ when StSelfValue <= 10:
                    {
                        SelfValueSB.Append("\n Высокие значения (8-10 стенов) принадлежат человеку, высоко оценивающему свой духовный потенциал, богатство своего внутреннего мира, человек склонен воспринимать себя как индивидуальность и высоко ценить собственную неповторимость. Уверенность в себе помогает противостоять средовым воздействиям, рационально воспринимать критику в свой адрес. ");
                        break;
                    }
            }
            SelfValueSB.Append("\n");
            SelfValueSB.Append("--------------------\n");

            SelfAcceptanceSB.Append("Самопринятие:");
            switch (StSelfAcceptance)
            {
                case int _ when StSelfAcceptance <= 3:
                    {
                        SelfAcceptanceSB.Append("\n Низкие значения (1-3 стена) указывают на общий негативный фон восприятия себя, на склонность воспринимать себя излишне критично. Симпатия к себе недостаточно выражена, проявляется эпизодически. Негативная оценка себя существует в разных формах: от описания себя в комическом свете до самоуничижения.");
                        break;
                    }
                case int _ when StSelfAcceptance <= 7:
                    {
                        SelfAcceptanceSB.Append("\n Средние значения (4-7 стенов) отражают избирательность отношения к себе. Человек склонен принимать не все свои достоинства и критиковать не все свои недостатки. ");
                        break;
                    }
                case int _ when StSelfAcceptance <= 10:
                    {
                        SelfAcceptanceSB.Append("\n Высокие значения (8-10 стенов) характеризуют склонность воспринимать все стороны своего \"Я\", принимать себя во всей полноте поведенческих проявлений. Общий фон восприятия себя положительный. Человек часто ощущает симпатию к себе, ко всем качествам своей личности. Свои недостатки считает продолжением достоинств. Неудачи, конфликтные ситуации не дают основания для того, чтобы считать, себя плохим человеком.");
                        break;
                    }
            }
            SelfAcceptanceSB.Append("\n");
            SelfAcceptanceSB.Append("--------------------\n");

            SelfAttachmentSB.Append("Самопривязанность: ");
            switch (StSelfAttachment)
            {
                case int _ when StSelfAttachment <= 3:
                    {
                        SelfAttachmentSB.Append("\n Низкие значения (1-3 стена) фиксируют высокую готовность к изменению \"Я\"-концепции, открытость новому опыту познания себя, поиски соответствия реального и идеального \"Я\". Желание развивать и совершенствовать собственное \"Я\" ярко выражено, источником чего может быть, неудовлетворенность собой. Легкость изменения представлений о себе.");
                        break;
                    }
                case int _ when StSelfAttachment <= 7:
                    {
                        SelfAttachmentSB.Append("\n Средние значения (4-7 стенов) указывают на избирательность отношения к своим личностным свойствам, на стремление к изменению лишь некоторых своих качеств при сохранении прочих других.");
                        break;
                    }
                case int _ when StSelfAttachment <= 10:
                    {
                        SelfAttachmentSB.Append("\n Высокие значения (8-10 стенов) отражают высокую ригидность \"Я\"-концепции, стремление сохранить в неизменном виде свои качества, требования к себе, а главное - видение и оценку себя. Ощущение самодостаточности и достижения идеала мешает реализации возможности саморазвития и самосовершенствования. Помехой для самораскрытия может быть также высокий уровень личностной тревожности, предрасположенность воспринимать окружающий мир как угрожающий самооценке. ");
                        break;
                    }
            }
            SelfAttachmentSB.Append("\n");
            SelfAttachmentSB.Append("--------------------\n");

            InternalConflictSB.Append("Внутренняя конфликтность:");
            switch (StInternalConflict)
            {
                case int _ when StInternalConflict <= 3:
                    {
                        InternalConflictSB.Append("\n Низкие значения (1-3 стена) наиболее часто встречаются у тех, кто в целом положительно относится к себе, ощущает баланс между собственными возможностями и требованиями окружающей реальности, между притязаниями и достижениями, доволен сложившейся жизненной ситуацией и собой. При этом возможны отрицание своих проблем и поверхностное восприятие себя.");
                        break;
                    }
                case int _ when StInternalConflict <= 7:
                    {
                        InternalConflictSB.Append("\n Средние значения (4-7 стенов) характерны для человека, у которого отношение к себе, установка видеть себя зависит от степени адаптированности в ситуации. В привычных для себя условиях, особенности которых хорошо знакомы и прогнозируемы, наблюдаются положительный фон отношения к себе, признание своих достоинств и высокая оценка своих достижений. Неожиданные трудности, возникающие дополнительные препятствия могут способствовать усилению недооценки собственных успехов.");
                        break;
                    }
                case int _ when StInternalConflict <= 10:
                    {
                        InternalConflictSB.Append("\n Высокие значения (8-10 стенов) соответствуют человеку, у которого преобладает негативный фон отношения к себе. Он находится в состоянии постоянного контроля над своим \"Я\", стремится к глубокой оценке всего, что происходит в его внутреннем мире. Развитая рефлексия переходит в самокопание, приводящее к нахождению осуждаемых в себе качеств и свойств. Отличается высокими требованиями к себе, что нередко приводит к конфликту между \"Я\" реальным и \"Я\" идеальным, между уровнем притязаний и фактическими достижениями, к признанию своей малоценности. Истинным источником своих достижений и неудач считает преимущество себя. ");
                        break;
                    }
            }
            InternalConflictSB.Append("\n");
            InternalConflictSB.Append("--------------------\n");



           SelfBlameSB.Append("Самообвинение: ");
           switch (StSelfBlame)
            {
                case int _ when StSelfBlame <= 3:
                    {
                        SelfBlameSB.Append("\n Низкие значения (1-3 стена) обнаруживают тенденцию к отрицанию собственной вины в конфликтных ситуациях. Защита собственного \"Я\" осуществляется путем обвинения преимущественно других, перенесением ответственности на окружающих за устранение барьеров на пути к достижению цели. Ощущение удовлетворенности собой сочетания с порицанием других, поисками в них источников всех неприятностей и бед.");
                        break;
                    }
                case int _ when StSelfBlame <= 7:
                    {
                        SelfBlameSB.Append("\n Средние значения (4-7 стенов) указывают на избирательное отношение к себе. Обвинение себя за те или иные поступки и действия сочетается с выражением гнева, досады в адрес окружающих.");
                        break;
                    }
                case int _ when StSelfBlame <= 10:
                    {
                        SelfBlameSB.Append("\n Высокие значения (8-10 стенов) можно наблюдать у тех, кто видит в себе прежде всего недостатки, кто готов поставить себе в вину все свои промахи и неудачи. Проблемные ситуации, конфликты в сфере общения актуализируют сложившиеся психологические защиты, среди которых доминируют реакции защиты собственного \"Я\" в виде порицания, осуждения себя или привлечения смягчающих обстоятельств. Установка на самообвинение сопровождается развитием внутреннего напряжения, ощущением невозможности удовлетворения основных потребностей.");
                        break;
                    }
            }
            SelfBlameSB.Append("\n");
            SelfBlameSB.Append("--------------------");

            StringBuilder ResultSB = new StringBuilder();
            ResultSB.Append(ClosenessSB);
            ResultSB.Append(SelfConfidenceSB);
            ResultSB.Append(SelfGuidanceSB);
            ResultSB.Append(SelfAttitudeSB);
            ResultSB.Append(SelfValueSB);
            ResultSB.Append(SelfAcceptanceSB);
            ResultSB.Append(SelfAttachmentSB);
            ResultSB.Append(InternalConflictSB);
            ResultSB.Append(SelfBlameSB);
            return ResultSB.ToString();
        }


        //Constant list of MIS questions
        public static readonly string[] ListOfQuestions =
        {
            "1.Мои слова довольно редко расходятся с делами.",
"2.Случайному человеку я, скорее всего, покажусь человеком приятным.",
"3.К чужим проблемам я всегда отношусь с тем же пониманием, что и к своим.",
"4.У меня нередко возникает чувство, что то, о чем я с собой мысленно разговариваю, мне неприятно.",
"5.Думаю, что все мои знакомые относятся ко мне с симпатией.",
"6.Самое разумное, что может сделать человек в своей жизни, это не противиться собственной судьбе.",
"7.У меня достаточно способностей и энергии воплотить в жизнь задуманное.",
"8.Если бы я раздвоился, то мне было бы довольно интересно общаться со своим двойником.",
"9.Я не способен причинять душевную боль самым любимым и родным мне людям.",
"10.Я считаю, что не грех иногда пожалеть самого себя.",
"11.Совершив какой-то промах, я часто не могу понять, как же мне могло прийти в голову, что из задуманного могло получиться что-нибудь хорошее.",
"12.Чаще всего я одобряю свои планы и поступки.",
"13.В моей личности есть, наверное, что-то такое, что способно вызвать у других острую неприязнь.",
"14.Когда я пытаюсь оценить себя, я прежде всего вижу свои недостатки.",
"15.У меня не получается быть для любимого человека интересным длительное время.",
"16.Можно сказать, что я ценю себя достаточно высоко.",
"17.Мой внутренний голос редко подсказывает мне то, с чем бы я в конце концов не согласился.",
"18.Многие мои знакомые не принимают меня так уж всерьез.",
"19.Бывало, и не раз, что я сам остро ненавидел себя.",
"20.Мне очень мешает недостаток энергии, воли и целеустремленности.",
"21.В моей жизни возникали такие обстоятельства, когда я шел на сделку с собственной совестью.",
"22.Иногда я сам себя плохо понимаю.",
"23.Порой мне бывает мучительно больно общаться с самим собой.",
"24.Думаю, что без труда смог бы найти общий язык с любым разумным и знающим человеком.",
"25.Если я и отношусь к кому-нибудь с укоризной, то прежде всего к самому себе.",
"26.Иногда я сомневаюсь, можно ли любить меня по-настоящему.",
"27.Нередко мои споры с самим собой обрываются мыслью, что все равно выйдет не так, как я решил.",
"28.Мое отношение к самому себе можно назвать дружеским.",
"29.Вряд ли найдутся люди, которым я не по душе.",
"30.Часто я не без издевки подшучиваю над собой.",
"31.Если бы мое второе «Я» существовало, то для меня это был бы довольно скучный партнер по общению.",
"32.Мне представляется, что я достаточно сложился как личность, и поэтому не трачу много сил на то, чтобы в чем-то стать другим.",
"33.В целом меня устраивает то, какой я есть.",
"34.К сожалению, слишком многие не разделяют моих взглядов на жизнь.",
"35.Я вполне могу сказать, что уважаю сам себя.",
"36.Я думаю, что имею умного и надежного советчика в себе самом.",
"37.Сам у себя я довольно часто вызываю чувство раздражения.",
"38.Я часто, но довольно безуспешно пытаюсь в себе что-то изменить.",
"39.Я думаю, что моя личность гораздо интереснее и богаче, чем это может показаться на первый взгляд.",
"40.Мои достоинства вполне перевешивают мои недостатки.",
"41.Я редко остаюсь непонятым в самом важном для меня.",
"42.Думаю, что другие в целом оценивают меня достаточно высоко.",
"43.То, что со мной случается,- это дело моих собственных рук.",
"44.Если я спорю с собой, то всегда уверен, что найду единственно правильное решение.",
"45.Когда со мной случаются неприятности, как правило, я говорю: «И поделом тебе».",
"46.Я не считаю, что достаточно духовно интересен для того, чтобы быть притягательным для многих людей.",
"47.У меня нередко возникает сомнения: а таков ли я на самом деле, каким себе представляюсь?",
"48.Я не способен на измену даже в мыслях.",
"49.Чаще всего я думаю о себе с дружеской иронией.",
"50.Мне кажется, что мало кто может подумать обо мне плохо.",
"51.Уверен, что на меня можно положиться в самых ответственных делах.",
"52.Я могу сказать, что в целом я контролирую свою судьбу.",
"53.Я никогда не выдаю понравившиеся мне чужие мысли за свои.",
"54.Каким бы я ни казался окружающим, я то знаю, что в глубине души я лучше, чем большинство других.",
"55.Я хотел бы оставаться таким, какой я есть.",
"56.Я всегда рад критике в свой адрес, если она обоснована и справедлива.",
"57.Мне кажется, что если бы таких людей, как я, было больше, то жизнь изменилась бы в лучшую сторону.",
"58.Мое мнение имеет достаточный вес в глазах окружающих.",
"59.Что-то мешает мне понять себя по-настоящему.",
"60.Во мне есть немало такого, что вряд ли вызывает симпатию.",
"61.В сложных обстоятельствах я обычно не жду, пока проблемы разрешатся сами собой.",
"62.Иногда я пытаюсь выдать себя не за того, кто я есть.",
"63.Быть снисходительным к собственным слабостям - вполне естественно.",
"64.Я убедился, что глубокое проникновение в себя - малоприятное и довольно рискованное занятие.",
"65.Я никогда не раздражаюсь и не злюсь без особых на то причин.",
"66.У меня бывают такие моменты, когда я понимал, что меня есть за что презирать.",
"67.Я часто чувствую, что мало влияю на то, что со мной происходит.",
"68.Именно богатство и глубина моего внутреннего мира и определяют мою ценность как личности.",
"69.Долгие споры с собой чаще всего оставляют горький осадок в моей душе, чем приносят облегчение.",
"70.Думаю, что общение со мной доставляет людям искреннее удовольствие.",
"71.Если говорить откровенно, иногда я бываю очень неприятен.",
"72.Можно сказать, что я себе нравлюсь.",
"73.Я человек надежный.",
"74.Осуществление моих желаний мало зависит от везения.",
"75.Мое внутреннее «Я» всегда мне интересно.",
"76.Мне очень просто убедить себя не расстраиваться по пустякам.",
"77.Близким людям свойственно меня недооценивать.",
"78.У меня в жизни нередко бывают минуты, когда я сам себе противен.",
"79.Мне кажется, что я все-таки не умею злиться на себя по-настоящему.",
"80.Я убедился, что в серьезных делах на меня лучше не рассчитывать.",
"81.Порой мне кажется, что я какой-то странный.",
"82.Я не склонен пасовать перед трудностями.",
"83.Мое собственное «Я» не представляется мне чем-то достойным глубокого внимания.",
"84.Мне кажется, что, глубоко обдумывая свои внутренние проблемы, я научился гораздо лучше себя понимать.",
"85.Сомневаюсь, что вызываю симпатию у большинства окружающих.",
"86.Мне случалось совершать такие поступки, которым вряд ли можно найти оправдание.",
"87.Где-то в глубине души я считаю себя слабаком.",
"88.Если я искренне и обвиняю себя в чем-то, то, как правило, обличительного запала хватает ненадолго.",
"89.Мой характер, каким бы он ни был, вполне меня устраивает.",
"90.Я вполне представляю себе, что меня ждет впереди.",
"91.Иногда мне бывает трудно найти общий язык со своим внутренним «Я».",
"92.Мои мысли о себе по большей части сводятся к обвинениям в собственный адрес.",
"93.Я не хотел бы сильно меняться даже в лучшую сторону, потому что каждое изменение - это потеря какой-то дорогой частицы самого себя.",
"94.В результате моих действий слишком часто получается совсем не то, на что я рассчитывал.",
"95.Вряд ли во мне есть что-то, чего бы я не знал.",
"96.Мне еще многого не хватает, чтобы с уверенностью сказать себе: «Да, я вполне созрел как личность».",
"97.Во мне вполне мирно уживаются как мои достоинства, так и мои недостатки.",
"98.Иногда я оказываю «бескорыстную» помощь людям только для того, чтобы лучше выглядеть в собственных глазах.",
"99.Мне слишком часто и безуспешно приходится оправдываться перед самим собой.",
"100.Те, кто меня не любит, просто не знают, какой я человек.",
"101.Убедить себя в чем-то не составляет для меня большого труда.",
"102.Я не испытываю недостатка в близких и понимающих меня людях.",
"103.Мне кажется, что мало кто уважает меня по-настоящему.",
"104.Если не мелочиться, то в целом меня не в чем упрекнуть.",
"105.Я сам создал себя таким, каков я есть.",
"106.Мнение других обо мне вполне совпадает с моим собственным.",
"107.Мне бы очень хотелось во многом себя переделать.",
"108.Ко мне относятся так, как я того заслуживаю.",
"109.Думаю, что моя судьба сложится все равно не так, как бы мне хотелось теперь.",
"110.Уверен, что в жизни я на своем месте."
        };
    }
}
