﻿using System.Windows;

namespace МИСиУСК
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MIS_Click(object sender, RoutedEventArgs e)
        {
            Test test = new Test();
            test.Max = 109;
            test.Questions = MISTestsAnalysis.ListOfQuestions;
            test.Answers = new int[110];
            test.Question.Text = test.Questions[0];
            test.Count.Text = "1/110";
            for (int i = 0; i < test.Answers.Length; i++) test.Answers[i] = -1;
            this.Close();
            test.ShowDialog();
            
        }

        private void USK_Click(object sender, RoutedEventArgs e)
        {
            Test test = new Test();
            
            test.Max = 43;
            test.Questions= USKTestsAnalysis.Questions;
            test.Answers = new int[44];
            test.Question.Text = test.Questions[0];
            test.Count.Text = "1/44";
            for (int i = 0; i < test.Answers.Length; i++) test.Answers[i] = -1;
            this.Close();
            test.ShowDialog();
        }
    }
}
